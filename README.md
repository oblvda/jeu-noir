# game-noir

## Nom
The Most Forgotten French Boy

## Description
Pour ce projet, j'ai utilisé TypeScript et HTML/CSS (avec, en partie, Bootstrap).

⚠️ Le jeu n'est pas tout à fait responsive, mais c'est quelque chose que j'envisage pour la suite.

## Fonctionnement du code
Mon code fonctionne ainsi: 

Pour chaque écran ("slide") de jeu, la fonction "displayScreen()" appelle des constantes correspondant chacune à un visuel à l'index indiqué. Par exemple, on appelle le personnage de Jean-Claude, le décor de l'hôtel, et le texte correspondant à l'index pour composer la première slide.

Les boutons de choix ne s'affichent que si ils sont contenus dans les objets du tableau "main" (ce qui est un paramètre optionnel). Ils déclenchent chacun la fonction "jumpToIndexFirstOption()" ou bien "jumpToIndexSecondOption()" selon le bouton de choix activé. Cette fonction ajoute un nombre pour aller à un index précis, qui part sur une autre branche du jeu.

Le bouton "suivant" appelle deux fonctions selon une condition: 
- La fonction "nextSlide()" est appelée seulement si les points de sortie, qui correspondent à des bouléens initialisés "true" ou "false" à chaque slide, sont à "true". 
- Dans le cas inverse (où le bouléen est équivalent à "false"), la fonction "endGame()" est appelée et termine le jeu. 

## Synopsis
Dominique est réceptionniste dans un grand hôtel de luxe dans le Sud de la France dans les années 80. Passionné de musique rock, son destin s'apprête à basculer lorsque que le manager de la plus célèbre rockstar française pénètre dans le hall et s'adresse à lui...

## Quelques visuels
![Ecran de début](screenshot.png)
![Exemple de slide](screenshot2.png)
![Exemple de slide](screenshot3.png)
![Exemple de slide](screenshot4.png)
