export interface Character{
    name:string,
    img:string
}

export interface Place{
    img:string
}

export interface Option{
    choice:string, 
    next:number,
}

export interface Slide{ 
    character:Character,
    place:Place,
    text:string,
    time?:string,
    option?:Option[],
    end:boolean
}