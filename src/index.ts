import { Character, Place, Slide} from "./entities"
import * as images from '../public/img/*.png'


// Personnages
const dominiqueRocker:Character = {
    name: "Dominique",
    img:images.DominiqueRocker
}

const dominiqueBassGuitar:Character = {
    name: "Dominique",
    img:images.DominiqueBassGuitar
}

const dominiqueGroom:Character = {
    name: "Dominique",
    img:images.DominiqueGroom
}

const jeanClaude:Character = {
    name: "Jean-Claude",
    img:images.JeanClaude
}

const johnny:Character = {
    name: "Johnny",
    img:images.Johnny
}

const patrice:Character = {
    name: "Patrice le fan",
    img:images.Patrice
}

const sylvie:Character = {
    name: "Sylvie la fan",
    img:images.Sylvie
}

const clarisse:Character = {
    name: "Clarisse",
    img:images.Clarisse
}

// Lieux
const hotel:Place = {
    img:images.hotel
}

const sceneConcert:Place = {
    img:images.scene
}

const lodge:Place = {
    img:images.lodge
}

let currentSlide = 0
let main:Slide[] = [ 
// Scène 1
    {character: jeanClaude,
    place: hotel,
    text: "Bonjour ! Passez-moi Johnny ! Je suis son manager !",
    time: "1985-06-20, 14:16",
    end:true},
    {character: dominiqueGroom,
    place: hotel,
    text: "Tout de suite !",
    end:true},
    {character: jeanClaude,
    place: hotel,
    text: "Allô, Johnny, ça va ? Bien ! Bon, on a un gros problème pour ce soir. Mike est toujours en prison !... Oui... Oui. Je vais voir ce que je peux faire.",
    end:true},
    {character: dominiqueRocker,
    place: hotel,
    text: "Tout va bien ? J'ai fini mon service.",
    end:true},
    {character: jeanClaude,
    place: hotel,
    text: "Ouah! T’as le look ! Tu peux peut-être m'aider. Tu sais jouer de la guitare ?",
    end:true},
    {character: dominiqueRocker,
    place: hotel,
    text: "Merci ! Mais non, je ne sais pas jouer...",
    end:true},
    {character: jeanClaude,
    place: hotel,
    text: "Pas grave ! Ça te dirait de jouer avec nous au moins pour ce soir, et peut-être pour les trois autres dates qui suivent ? Juste le temps que le bassiste sorte de prison.",
    end:true,
    option: [
        {choice: "Vous acceptez.", next: 1},
        {choice:  "Vous refusez.", next: 23}
        ],
    },
    {character: dominiqueRocker,
    place: hotel,
    text: "Pourquoi pas ! Mais encore une fois, je ne sais pas jouer de la guitare !...",
    end:true},
    {character: jeanClaude,
    place: hotel,
    text: "Ça, c’est pas grave, ça peut s’arranger ! C’est juste pour être bassiste. On te met dans un coin de la scène, pas trop éclairé. Tu fais semblant, et ça va le faire ! Si ça te dit, rendez-vous pour la répétition à dix-sept heures, au Zénith. Tu demandes Jean-Claude. C’est moi !",
    end:true},
// Scène 2
    {character: jeanClaude,
    place: lodge,
    text: "Tiens, voilà notre nouveau bassiste ! En forme ?... Bon, allez, tout le monde en scène ! On va faire un essai son et lumière !",
    time: "1985-06-20, 17:02",
    end:true},
    {character: dominiqueBassGuitar,
    place: sceneConcert,
    text: "...",
    end:true},
    {character: jeanClaude,
    place: sceneConcert,
    text: "Voilà, tu te mets là, tu branches ta guitare sur cet ampli et tu peux bouger, mais pas trop, histoire de ne pas être en pleine lumière, pour pas que le public te voie trop et que les photographes ne puissent pas te prendre en photo. Jusqu’aux choristes, là ! Pas plus ! Vas-y, bouge ! Fais semblant de jouer ! Reste dans le rythme !",
    end:true},
    {character: dominiqueBassGuitar,
    place: sceneConcert,
    text: "Même en ne jouant pas pour de vrai, c'est pas évident...",
    end:true},
    {character: jeanClaude,
    place: sceneConcert,
    text: "Alors, pour moi, ça colle. C’est bon ! Et toi, ça t’a plu ? Tu penses pouvoir le faire ? Ce soir ? Et les autres soirs ? Tu touches deux cent francs par soir et tu manges avec nous après chaque concert ! Tu peux même dormir avec nous si t’arrives à emballer une choriste (rires). Mais bon, c’est duraille, je sais de quoi je parle !",
    end:true,
    option: [
        {choice: "Vous acceptez.", next: 1},
        {choice: "Vous refusez.", next: 17}
        ]
    },
// Scène 3
    {character: johnny,
    place: sceneConcert,
    text: "Bonsoir public!",
    time: "1985-06-20, 20:40",
    end:true},
    {character: patrice,
    place: sceneConcert,
    text: "C’est Johnny ! Il est là !!! JOHNNY !!! Regarde-moi, Johnny !",
    end:true},
    {character: sylvie,
    place: sceneConcert,
    text: "Mais qui est ce nouveau bassiste ? Ce n’est pas le même qu’à Marseille...",
    end:true},
    {character: dominiqueBassGuitar,
    place: sceneConcert,
    text: "...",
    end:true},
    {character: jeanClaude,
    place: lodge, 
    text:"Alors, ces débuts? Bien? Moi je suis content de toi en tout cas. Tu continues la tournée ? Dans le coin, on joue encore à Avignon demain, Aix vendredi et Istres samedi ! Après, j’espère bien qu’on aura sorti Mike de prison (rires). Sinon, il va falloir que t’apprennes à vraiment jouer de la basse !",
    time: "1985-06-20, 23:42",
    end:true,
    option: [
        {choice: "Vous acceptez.", next: 1},
        {choice: "Vous refusez.", next: 13}
        ]
    },
// Scène 4 
    {character: dominiqueRocker,
    place: lodge,
    text: "C’est le top ! Je continue jusqu’à samedi !",
    end:true},
    {character: clarisse,
    place: lodge,
    text: "Hey pretty face ! Alors, qui t'es toi ? Tu remplaces Mike ? Tu joues drôlement bien (rires) ! Tu restes avec nous pour toute la suite de la tournée ?",
    end:true},
    {character: dominiqueRocker,
    place: lodge,
    text: "Je m'appelle Dominique, enchanté !",
    end:true,
    option: [
        {choice: "Vous tentez une approche.", next: 11},
        {choice: "Vous lui parlez.", next: 1}
        ]},
    {character: clarisse,
    place: lodge,
    text: "Moi, c'est Clarisse de... Memphis ! Je suis choriste, à l'arrière de la scène, mais ça ne veut pas dire que je n'ai pas de talent...",
    end:true},
    {character: jeanClaude,
    place: lodge,
    text: "Bon boulot Dominique, je vais me coucher. On se dit à demain soir !",
    end:true},
    {character: dominiqueRocker,
    place: lodge,
    text: "À demain Jean-Claude !",
    end:true},
    {character: clarisse,
    place: lodge,
    text: "Enfin débarassés de lui ! Bon, qu'est-ce qu'on fait ?",
    end:true,
    option: [
        {choice: "Vous tentez une approche.", next: 1},
        {choice: "Vous allez vous coucher.", next: 10}
        ]},

// Scène 5
    {character: clarisse,
    place: lodge,
    text: "Ouahhh ! Quelle fiesta hier soir ! Ça, c’était une dernière !",
    time: "1985-06-20, 14:35",
    end:true},
    {character: dominiqueRocker,
    place: lodge,
    text: "Clarisse, c’était vraiment super ! Maintenant, je dois y aller. Il faut que je sois au boulot à l’hôtel à quinze heures ! On se revoit bientôt ?",
    end:true},
    {character: clarisse,
    place: lodge,
    text: "Bah, je sais pas trop! The show must go on, comme on dit en Amérique ! Vas-y, et n’oublie pas tes cadeaux !",
    end:false},
//Fin

//Route 1
    {character: dominiqueRocker,
    place: hotel,
    text: "Puisque je vous dis que je ne sais pas jouer de la guitare !... J'y vais, je vous souhaite bonne chance pour la suite !",
    end:false},

//Route 2
    {character: dominiqueBassGuitar,
    place: sceneConcert,
    text: "Je le sens pas trop, j'ai le coeur qui tape et envie de vomir... Je vais plutôt rentrer chez moi...",
    end:false},

// Route 3
    {character: dominiqueRocker,
    place: lodge,
    text: "J'ai l'impression de ne pas être honnête avec le public... J'ai peur de me faire démasquer. Je vais plutôt partir et reprendre le cours de ma vie.",
    end:false},

// Route 4
    {character: jeanClaude,
    place: lodge,
    text: "Qu'est-ce que tu crois que tu fais là ?",
    end:true},
    {character: dominiqueRocker,
    place: lodge,
    text: "Euh...",
    end:true},
    {character: jeanClaude,
    place: lodge,
    text: "Garçon, c’était ta dernière avec nous ! Clarisse, pas touche ! C’est ma fiancée, on doit se marier cet été ! Alors, tu dégages ! Pour ton argent, comme tu t’es  payé sur la bête, c’est ceinture !",
    end:false},

// Route 5 
    {character: dominiqueRocker,
    place: lodge,
    text: "Je ne vais pas faire long feu non plus, je travaille demain. Bonne nuit, Clarisse !",
    end:true},
    {character: dominiqueRocker,
    place: lodge,
    text: "Hola ! Faut que je me grouille, moi ! Finie la vie de bohême. Je dois être à l’hôtel à 15 heures !",
    time: "1985-06-20, 14:35",
    end:false}
]

// Fonctions d'affichage de l'écran de fin
const endScreen = document.querySelector<HTMLElement>("#endScreen")
function endGame(){
    if (endScreen && gameScreen !== null){
        gameScreen.style.display = "none";
        endScreen.style.display = "block";
    }
}

// Fonctionnalité du bouton start
const startButton = document.querySelector<HTMLButtonElement>("#startBtn")
startButton?.addEventListener('click', () =>{
    startGame();
    displayScreen();
    }
)

// Démarrer le jeu: fait disparaître l'écran de début au profit du premier écran de jeu
const startScreen = document.querySelector<HTMLElement>("#startScreen")
function startGame(){
    if (startScreen !== null){
        startScreen.style.display = "none";
    }
}

// Affiche l'écran de jeu selon l'index
const gameScreen = document.querySelector<HTMLElement>("#gameScreen")
function displayScreen(){
    if (gameScreen !== null){
        gameScreen.style.display = "block";
        activeCharacter.style.backgroundImage = 'url('+String(main[currentSlide].character.img)+')'
        gameScreen.style.backgroundImage = 'url('+String(main[currentSlide].place.img)+')';
        name.innerText = String(main[currentSlide].character.name)
        dialogueText.innerText = String(main[currentSlide].text)
        timeAndDate.innerText = String(main[currentSlide]?.time)
        if(main[currentSlide].time?.length){
            timeAndDate.style.display = "flex"
        }else{
            timeAndDate.style.display = "none"
        }
    }
}

// Affichage du personnage actif
const activeCharacterGoesHere = document.querySelector<HTMLElement>("#activeCharacterGoesHere")

const activeCharacter = document.createElement("div")
activeCharacterGoesHere?.appendChild(activeCharacter)
activeCharacterGoesHere?.classList.add("justify-content-center")
activeCharacter.classList.add("character", "d-flex", "col-4")
activeCharacter.style.backgroundImage = 'url('+String(main[currentSlide].character.img)+')'

// Affichage de la boîte de dialogue
const scene = document.querySelector<HTMLDivElement>("#scene")
const nextButtonGoesHere = document.querySelector<HTMLDivElement>("#nextButtonGoesHere")
const optionButtons = document.querySelector<HTMLDivElement>("#optionsGoHere")
let current = main[currentSlide]

const name = document.createElement("h5")
scene?.appendChild(name)
name.classList.add("name")
name.innerText = String(main[currentSlide].character.name)

const timeAndDate = document.createElement("p")
if(current.time){
    scene?.appendChild(timeAndDate)
    timeAndDate.classList.add("time")
    timeAndDate.innerText = String(main[currentSlide]?.time)
}

const dialogueText = document.createElement("p")
scene?.appendChild(dialogueText)
dialogueText.classList.add("text")
dialogueText.innerText = String(main[currentSlide].text)

const nextButton = document.createElement("button")
nextButtonGoesHere?.appendChild(nextButton)
nextButton.classList.add("nextButton", "col-2")
nextButton.innerText = "Suivant"

const option1 = document.createElement("button")
const option2 = document.createElement("button")
optionButtons?.appendChild(option1)
option1.classList.add("option", "col-md-2")
optionButtons?.appendChild(option2)
option2.classList.add("option", "col-md-2")

if(current.option){
option1.innerText = String(current.option[0].choice)
option2.innerText = String(current.option[1].choice)
}

// Affichage des boutons d'option
function displayOptions(){
    if(current.option){
        option1.innerText = String(current.option[0].choice)
        option2.innerText = String(current.option[1].choice)
    }
    if(main[currentSlide].option?.length){
        option1.style.display = "flex"
        option2.style.display = "flex"
        nextButton.style.display = "none"
    }else{
        option1.style.display = "none"
        option2.style.display = "none"
        nextButton.style.display = "flex"
    }
}

// Affichage des options et du bouton suivant en fonction de si l'un ou l'autre est affiché.
if(main[currentSlide].option !== undefined){
    option1.style.display = "flex"
    option2.style.display = "flex"
    nextButton.style.display = "none"
}else{
    option1.style.display = "none"
    option2.style.display = "none"
    nextButton.style.display = "flex"
}

// Fonctionnalité du bouton restart
const restartButton = document.querySelector<HTMLButtonElement>("#restartBtn")
restartButton?.addEventListener('click', () =>{
    restartGame()
    }
)

// Redémarrer le jeu: fait disparaître l'écran de fin au profit du premier écran de jeu
function restartGame(){
    currentSlide = 0
    current = main[currentSlide]
    displayScreen()
    if (endScreen && gameScreen !== null){
        endScreen.style.display="none"
        gameScreen.style.display="block"
    }
}

// Finir l'histoire aux points de sortie
function exitPoints(){
    if (current.end == false){
        endGame()
    } else nextSlide()
}

// Fonction pour passer à la slide suivante en parcourant le tableau principal
function nextSlide(){
    currentSlide++
    current = main[currentSlide]
    if (currentSlide < main.length){
        displayScreen()
        displayOptions()
    } else return 
}

// Fonction pour le choix afin de passer à l'index indiqué de la slide 
function jumpToIndexFirstOption(){
    if (current.option){
        currentSlide+=Number(current.option[0].next)
    }
    current = main[currentSlide]
    if (currentSlide < main.length){
        displayScreen()
        displayOptions()
    } else return
}

// Fonction pour le choix afin de passer à l'index indiqué de la slide 
function jumpToIndexSecondOption(){
    if (current.option){
        currentSlide+=Number(current.option[1].next)
    }
    current = main[currentSlide]
    if (currentSlide < main.length){
        displayScreen()
        displayOptions()
    } else return
}

// Fonctionnalités du bouton suivant
nextButton?.addEventListener('click', () =>{
    exitPoints()
    }
)

// Fonctionnalités du bouton d'option 1
option1?.addEventListener('click', () => {
    jumpToIndexFirstOption()
    }
)

// Fonctionnalités du bouton d'option 2
option2?.addEventListener('click', () => {
    jumpToIndexSecondOption()
    }
)